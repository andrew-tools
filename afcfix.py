#!/usr/bin/python
# -*- coding: utf-8  -*-
"""
This is not a complete bot; rather, it is a template from which simple
bots can be made. You can rename it to mybot.py, then edit it in
whatever way you want.

The following parameters are supported:

&params;

-dry              If given, doesn't do any real changes, but only shows
                  what would have been changed.

All other parameters will be regarded as part of the title of a single page,
and the bot will only work on that single page.
"""
#
# (C) Pywikipedia bot team, 2006-2010
#
# Distributed under the terms of the MIT license.
#
__version__ = '$Id: basic.py 8278 2010-06-11 17:01:24Z xqt $'
#

import wikipedia as pywikibot
import pagegenerators
import re

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {
    '&params;': pagegenerators.parameterHelp
}

class BasicBot:
    # Edit summary message that should be used.
    # NOTE: Put a good description here, and add translations, if possible!
    msg = {
        'en': u'Robot correcting misplaced [[WP:WPAFC|AFC]] submission',
    }

    def __init__(self, generator, dry):
        """
        Constructor. Parameters:
            * generator - The page generator that determines on which pages
                          to work on.
            * dry       - If True, doesn't do any real changes, but only shows
                          what would have been changed.
        """
        self.generator = generator
        self.dry = dry
        self.perdry = dry
        # Set the edit summary message
        self.summary = pywikibot.translate(pywikibot.getSite(), self.msg)

    def load(self, page):
        """
        Loads the given page, does some changes, and saves it.
        """
        try:
            # Load the page
            text = page.get()
        except pywikibot.NoPage:
            pywikibot.output(u"Page %s does not exist; skipping."
                             % page.aslink())
        except pywikibot.IsRedirectPage:
            pywikibot.output(u"Page %s is a redirect; tagging."
                             % page.aslink())
	    page.put(u'{{db-g6}}', comment = "Robot tagging for G6")
        else:
            return text
        return None

    def run(self):
        for page in self.generator:
            self.treat(page)

    def treat(self, page):

	# Checking for speedies
	pgtext = self.load(page)

	wiki_e = re.search("{{db-", pgtext)

        try:
            wiki_e.group(0)
            pywikibot.output(u'Page %s not saved, has tag.' % page.aslink())	
	except AttributeError:
            tmp = ""
            if page.namespace() == 0: 
                # Bingo, misplaced.              
                #tmp = page.titleWithoutNamespace()
                #tmp = tmp.replace(tmp.split("/")[0]+"/","")
                tg = page.title()
                tmp = "Wikipedia talk:Articles for creation/" + tg
	        if page.title() == "Wikipedia:Files for upload":
        	    self.dry = True
	            self.doNotMove = True

        	if page.title() == "Wikipedia:Articles for creation/Redirects":
	            self.dry = True
        	    self.doNotMove = True
	        else:
        	    self.dry = self.perdry
	            self.doNotMove = False
	    else:
                return

	    if self.dry:
                if tmp:
                   if not self.doNotMove:
                      print "I would have moved " + tg + " to: " + tmp
	    else:
		if page.title() == "Wikipedia:Files for upload":
                    return
                if page.title() == "Wikipedia:Articles for creation/Redirects":
                    return
		print "Moving " + page.aslink() + " to " + tmp
		pg2 = pywikibot.Page(pywikibot.getSite(), page.title())
                page.move(tmp, self.summary, throttle=True)
		pg2.put(u'{{db-r2}}', comment = "Robot tagging for R2")

def main():
    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    genFactory = pagegenerators.GeneratorFactory()
    # The generator gives the pages that should be worked upon.
    gen = None
    # This temporary array is used to read the page title if one single
    # page to work on is specified by the arguments.
    pageTitleParts = []
    # If dry is True, doesn't do any real changes, but only show
    # what would have been changed.
    dry = False

    # Parse command line arguments
    for arg in pywikibot.handleArgs():
        if arg.startswith("-dry"):
            dry = True
        else:
            # check if a standard argument like
            # -start:XYZ or -ref:Asdf was given.
            if not genFactory.handleArg(arg):
                pageTitleParts.append(arg)

    if pageTitleParts != []:
        # We will only work on a single page.
        pageTitle = ' '.join(pageTitleParts)
        page = pywikibot.Page(pywikibot.getSite(), pageTitle)
        gen = iter([page])

    if not gen:
        gen = genFactory.getCombinedGenerator()
    if gen:
        # The preloading generator is responsible for downloading multiple
        # pages from the wiki simultaneously.
        gen = pagegenerators.PreloadingGenerator(gen)
        bot = BasicBot(gen, dry)
        bot.run()
    else:
        pywikibot.showHelp()

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()
